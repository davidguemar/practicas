
public class Ejer_06 {	


	public static void main(String[] args) {

		int [] vector = {1,2,6,4,5,6,4,1,2,3,4,5};


		System.out.println(contarIntConsecutivos(vector));

	}

	public static int contarIntConsecutivos (int [] consecutivos) {

		int contador = 0;
		int j;
		int contadorMax = 0;

		for (int i = 0 ; i < consecutivos.length-1; i++ ) {

			if (consecutivos[i] == consecutivos[i + 1] -1  ) {


				for ( j=i; j < consecutivos.length-1; j++ ) {

					if (consecutivos[j] == consecutivos[j + 1] -1) {
						contador++; 
					}else {
						break;
					}

				} 
				i = j;				
				
			}
			if (contador>= contadorMax)	{
				contadorMax = contador;
				contador = 0;
			}

		}
		if (contadorMax != 0) {
			contadorMax++;
		}

		return contador +1 ;

	}

}
