

/**
 * 
 * Array 10 elementos 
 * Programa simple que utiliza un array de diez elementos e 
 * inicializa cada elemento con valor 7
 * 21/1/21
 *
 */
import java.util.Scanner;

public class Ejer_01 {

	public static void main(String[] args) {
		
		int[] arraySeven = {7,7,7,7,7,7,7,7,7,7};// Se declara, dimensiona e inicializa simultaneamente
		
		for (int i=0;i<arraySeven.length;i++) { //Inicializaci�n del bucle for, que con la propiedad length que indica el tama�o del array
			System.out.println("Los diez elementos del primer bloque de arrays: arraySeven son, para el indice " +i+ " = "  + arraySeven[i]) ;
			
		}System.out.println();// separa un bucle de arrays de otro, �nicamente para que sea mas f�cil diferenciar un resultado, de otro en la consola de salida notiene funci�n 
		
		
				
		
		int[] arraySiete;// Se declara
		arraySiete= new int [] {7,7,7,7,7,7,7,7,7,7};// Se Crea, dimensiona e inicializa el array
		
		for (int i=0;i<arraySiete.length;i++) {//
			System.out.println("\nLos diez elementos del segundo bloque de arrays: arraySiete son, para el �ndice " +i+ " el valor " + arraySiete[i]);
			
		}System.out.println();//Separa un bucle de otro 
		
		
		
		
		int[] arraySiete_2=new int[10]; // Se declara y se dimensiona
				
		for (int i=0;i<arraySiete_2.length;i++) {
			arraySiete_2[i]=7; // Se asigna el valor a cada elemento del array
			System.out.println("Los diez elementos del tercer bloque de arrays: arraySiete_2 son, para el �ndice: " +i+ " el valor " + arraySiete_2[i]);
		}System.out.println();//Separa un bucle de otro 
		
		
		
		
		Scanner sc=new Scanner(System.in);// Clase, del paquete java.util* para falicitar la entrada de datos por pantalla 
		
		System.out.println("introduce el n�mero de elementos que desea que tenga el array a ser preferible 10");
		
		int numeroElementos=sc.nextInt(); // Se inicializa la variable 
		int[] arrayTeclado= new int[numeroElementos]; //La dimensi�n ser� el d�gito que introduzcamos por pantalla 
		
		for (int i=0;i<arrayTeclado.length;i++) {
			arrayTeclado[i]=7;// Se asigna el valor a cada elemento del array 
			System.out.println("Los elementos del �ltimo bloque de arrays: arrayTeclado son, para el �ndice: " +i+ " el valor " + arrayTeclado[i] );
			
			sc.close();
		}
		
		
		
	}

}
