/**
 * Array 20 elementos
 * 
 * Programa simple que utiliza un array de 20 elementos y
 *  los inicializa con el valor del indice multiplicado por 5
// *22/01/2021
 */
public class Ejer_02 {

	public static void main(String[] args) {
		
		int[] arrayVeinte=new int[20];// Declaración del array y dimensionado 
		
		for (int i=0;i<arrayVeinte.length;i++) {
			arrayVeinte[i]= i*5;// Se multiplica el valor del indice del array por cinco
			
			System.out.print( arrayVeinte[i] +" " );
		}		
		System.out.println();// Separa un bucle de otro 
		
		
		int[] arrayVeinte_2;
		arrayVeinte_2=new int [20];
		
		for (int i=0;i<arrayVeinte_2.length;i++) {
			arrayVeinte_2[i]= i*5;
			
			System.out.print(arrayVeinte_2[i] + " ");
		}
		

	}

}
