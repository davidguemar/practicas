package prueba;

public class Persona {

		private String nombre;
		private String apellidos;
		
		// Constructor
		public Persona() {
		}
		
		public Persona(String nombre, String apellidos) {
			this.nombre = nombre;
			this.apellidos = apellidos;
		}
		
		public Persona(String nombre) {
			this.nombre = nombre;
		}
		
		//Getters & Setters		
		public String getNombre() {
			return this.nombre;
		}
		
		public void setNombre(String n) {
			this.nombre = n;
		}
}
