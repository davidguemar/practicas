/**
 * 
 * Frecuencia de n�mero
 * programa que recibe un vector de enteros y un n�mero y decuelve
 * la frecuendia entre los elementos del vector del n�mero recibido
 *
 */
import java.util.Scanner;

public class Ejer_03 {

	public static void main(String[] args) {
		
		int[] lista = {1,1,1,1,2,2,3,3,3,3,4,4,4,};
		int numero = 3;
		                 // Esto es la invocaci�n del m�todo 
		int frecuencia = frecuenciaNumero(numero, lista);
		
		System.out.println(frecuencia);
		
	}
	     //Esto es la declarac�on de metodo constructor 
	public static int frecuenciaNumero(int numero, int[] listaNumeros) {
 		int contador = 0;
		  //Bucle que recorre el array y suma 1 cada vez que encuentra el n�mero introducido 
		for(int i=0; i < listaNumeros.length-1; i++) {
			if (listaNumeros[i] == numero) {
				contador++;
			}
		}
		
		return contador;
	}
	
	

}
