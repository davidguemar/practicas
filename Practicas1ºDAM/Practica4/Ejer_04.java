/**
 * 
 *  Vectores iguales
 *  Programa que dos vectores de enteros y devuelve true si son iguales
 *
 */
public class Ejer_04 {

	public static void main(String[] args) {
		int [] lista1 = {1,2,8,4,5,5};
		int [] lista2 = {1,2,3,4,5};
		
		                        // la invocacion del m�todo
		boolean iguales = Ejer_04.vectoresIntIguales(lista1, lista2);
		
		System.out.println(iguales);

	
		
	}
                       //la declaraci�n del m�todo
	public static boolean vectoresIntIguales(int [] _vector1, int []_vector2) {
		
		boolean resultado = false ;
		                   //lee el primer array
		for (int i = 0; i < _vector1.length-1; i++) {
			               //lee el segundo array			
			for (int j = 0; j < _vector2.length-1; j++) {
				          // compara si son iguales y devuelve true si lo son
				if (_vector1[i]  == _vector2[j]) {
					 resultado = true;
					  // si no son iguales devuelve false
				} else if (_vector1[i] != _vector2[j]){
					resultado = false;
					
				}
			}
		}
		return resultado;
		
		
	}

}
