/*
 * Comparar vectores
 * Programa que recibe dos vectores de caracteres y devuelve 0
 * si son iguales, 1 si el primero es mayor y -1 si el primero es menor 
 * 
 */



import java.util.Scanner;

public class Ejer_05 {

	static int vector1[];
	static int vector2[];

	public static void main(String[] args) {

		int contador = 0;

		Scanner teclado = new Scanner(System.in);
                    //Introducimos por pantalla el valor de los vectores 1 y 2 pedidos con un mensaje 
		System.out.println("Introduce la longitud del primer vector.");
		vector1 = new int [teclado.nextInt()];

		System.out.println("Introduce la longitud del segundo vector.");
		vector2 = new int [teclado.nextInt()];

		System.out.println("Introduce los valores del primer vector.\n");
		
		
                 //Repetimos el mensaje tantas veces como el valor del tama�o del vector 1 y 2 empezando por el indice 0
		for(contador = 0; contador < vector1.length; contador++) {
			System.out.println("Introduce un valor para la coordenada " + contador +" del primer vector.");
			vector1[contador] = teclado.next().charAt(0);
		}

		System.out.println("Introduce los valores del segundo vector.\n");

		for(contador = 0; contador < vector2.length; contador++) {
			System.out.println("Introduce un valor para la coordenada " + contador +" del segundo vector.");
			vector2[contador] = teclado.next().charAt(0);
		}   

		if (compararVectoresChar() == 0) {
			System.out.println("Los vectores son iguales.");
		}
		else if (compararVectoresChar() == -1){
			System.out.println("El primer vector es menor.");
		}
		else {
			System.out.println("El primer vector es mayor.");
		}

		teclado.close();

	}

	public static int compararVectoresChar() {

		int resultado = 0, contador = 0;

		for(contador = 0; contador < vector1.length && contador < vector2.length; contador++) {

			if (vector1[contador] < vector2[contador]) {
				resultado = 1;
				break;
			}
			else if (vector1[contador] > vector2[contador]) {
				resultado = -1;
				break;
			}
			else {
				resultado = 0;
			}

		}
		return resultado;
	}
}

