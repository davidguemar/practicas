//programa que muestra las tablas de multiplicar 

import java.util.*;
public class tablasDeMultiplicarEjer6 {

	public static void main(String[] args) {
		 calcularMostrarTabla();		

	}

	 static void calcularMostrarTabla() {
		
		
		Scanner sc =new Scanner(System.in);
		
		 boolean comporbar=true;
		 int numero;
		 
		 while(comporbar==true){
			 
			 
			 System.out.println("Introduce el n�mero ");
		  numero= sc.nextInt();
		  
		  
		  if (numero>0) {
			  
			  for (int i = 1; i <= 15; i++) {
				  
				  System.out.println(numero +" por " +i+ " es igual a " + numero*i);
				  
			  }
			  
			  
		  }else {
			  System.out.println("Este n�mero no es v�lido, escribe un n�mero mayor que cero");
		  }sc.close();
		
		 }
		 
		 
	}

}
