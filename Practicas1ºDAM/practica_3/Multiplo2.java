
// Multiplo2.java
	// Fecha: 01/12/2020
	// Autor/a:.......
	// Escribe en pantalla los m�ltiplos de 2 por debajo de un TOPE, 16.

	import java.util.Scanner;
	public class Multiplo2 	{
		public static void main(String argumentos[]) {
			mostrarMultiplos2();
		}
			static void mostrarMultiplos2() {
				
				// Declaraci�n de variables
			Scanner teclado = new Scanner(System.in);
			final int TOPE = 16  ; 				// Constante, el m�ximo valor del m�ltiplo
			int mult  ;							// Almacena el m�ltiplo calculado
			int cont  ;							// Contador utilizado en el c�lculo

			// Inicializa las variables
			mult = 0 ;
			cont = 0 ;

			System.out.println("\t M�ltiplos de 2\n");
			while (mult < TOPE)					// Bucle de c�lculo y visualizaci�n
			{
				mult = cont * 2;
				System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
				++cont;
			}
			teclado.close();
			}
			

			
		
	}