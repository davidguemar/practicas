
public class trianguloEjer9 {

	public static void main(String[] args) {
		
		triangulo(6);
		
	}

	static void triangulo(int numeroLineas) {
		
		for (int i = 0; i < numeroLineas; i++) {
			System.out.println(generaBlancos(numeroLineas - i) 
					+ generarAsteriscos(2*i+1));
		}
//		System.out.println(generaBlancos(numeroLineas -1)+generarAsteriscos(1));
//		System.out.println(generaBlancos(numeroLineas -2)+generarAsteriscos(3));
//		System.out.println(generaBlancos(numeroLineas -3)+generarAsteriscos(5));
//		System.out.println(generaBlancos(numeroLineas -4)+generarAsteriscos(7));
//		System.out.println(generaBlancos(numeroLineas -5)+generarAsteriscos(9));
//		System.out.println(generaBlancos(numeroLineas -6)+generarAsteriscos(11));
	}

	 static String generarAsteriscos(int cantidad) {
		 
         String resultado="";  
		 
		 for (int i=0; i<cantidad; i++) {
			  resultado+="*";
		 }			
		return resultado;
	}

	static String generaBlancos(int cantidad) {
		
		String resultado ="" ;
		
		for(int i=0;i<cantidad; i++)
		resultado+=" ";
		
		return resultado ;
	}
	

}
