//Programa que pide un n�mero e indica su posicion en la suceci�n de fibonacci


import java.util.Scanner;
public class fibonacciEjer8 {

	public static void main(String[] args) {
		fibonacci();

	}

	  static void fibonacci() {
		  Scanner teclado = new Scanner (System.in);
			int res = 1;
			System.out.print("Introduce un n�mero ");
			int num = teclado.nextInt();
			
			if (num > 1) { 
				res = (num - 1) + (num - 2);
				System.out.print("Fibonacci es = " + res);
			} else if (num == 1) {
				System.out.print("Fibonacci es = 1");
			} else if (num == 0) {
				System.out.print("Fibonacci es = 0");
			} else if (num < 0) {
				System.out.print("Fibonacci no admite negativos");
			}
		
	}

}
