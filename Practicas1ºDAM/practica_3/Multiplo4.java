import java.util.Scanner;

public class Multiplo4 {
	
	public static void main(String argumentos[]) {
		mostrarMultiplos2(pedirTope());
		
		/*int tope= pedirTope();
		
		mostrarMultiplos2(tope);    //Tope del metodo =tope
		
		int maximo=new Scanner(System.in).nextInt();
		
		mostrarMultiplos2(maximo);     //Tope del metodo =20*/
		
	}
	
	
	static int pedirTope() {
		System.out.println("Introduce el tope ");
		return new Scanner(System.in).nextInt();
		
	}
	
	static void mostrarMultiplos2(int topeDelMetodo) {

	// Declaraci�n de variables
		
	int mult  ;							// Almacena el m�ltiplo calculado
	int cont  ;							// Contador utilizado en el c�lculo

	// Inicializa las variables
	mult = 0 ;
	cont = 0 ;

	System.out.println("\t M�ltiplos de 2\n");
	while (mult < topeDelMetodo)					// Bucle de c�lculo y visualizaci�n
	{
		mult = cont * 2;
		System.out.println("\t  " +    '#' + (cont+1) + '\t' + mult);
		++cont;
	}
	
}

}
