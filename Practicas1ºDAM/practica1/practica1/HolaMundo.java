package practica1;
// EJERCICIO 1

/**
 * HolaMundo.java
 * Programa simple que muestra un mensaje la consola del sistema.
 * ajp - 2018.09.19
 */
/*
 * Esto es un bloque de comentrarios que el compilador ignora 
 * pero que sirve para entender el programa.
 */
public class HolaMundo {
	/* Es el encabezado de la clase. "HolaMundo" es el nombre principal
	 * del programa y debe coincidir con el nombre del fichero donde
	 * est� contenida."public class" son dos palabras reservadas del
	 * lenguaje para indicar: que se est� declarando una clase y que el 
	 * acceso a esa clase es p�blico.  
	 *  "{" Es la llave de apertura  y sirve para que el compilador sepa
	 *  donde empieza el bloque correspondiente a la clase que se est� 
	 *  indicando.
	 * 
	 */
	public static void main(String[] args) {
		/*
		 * main es el nombre del m�todo desde el que se empezar� a ejecutar
		 * el programa, aunque no es una palabra reservada no puede ejecutarse 
		 * otro nombre.
		 * "public static void" Son tres palabras reservadas
		 * obligatorias para indicar:  Que main() no devuelve resultado.
                                       Que main() siempre estar� disponible en el programa. 
                                       Que acceso a main() es p�blico. 
          "(String[] args)" es lista de argumentos que opcionalmente puede recibir el
           m�todo main() cuando se ejecute desde la l�nea de comandos. Hay que indicarla
           pero no se utiliza en programas sencillos como el ejemplo. 
          "{" es la llave de apertura, para indicar al compilador d�nde empieza el bloque
           de instrucciones correspondientes al m�todo main().
           

                            

		 */
		System.out.println("Hola mundo..."); 
		/* 
		 * es la sentencia que imprime un texto en la pantalla
		 * "System" es el sistema f�sico del ordenador
		 * ".out" es la pantalla. El punto indica que es relativo a 
		 * System
		 * ".println." es un m�todo que muestra un texto en la salida est�ndar
		 * "(HolaMundo..)" Es el argumento que contiene el texto a imprimir
		 * Debe ir entrecomillado si es literal.
		 * Puede ser una variable que contenga el mensaje.
		 * ";" Es el fin de una sentencia en java.
		 */
		System.out.println("El nombre del programa es HolaMundo"); 
		
		System.out.println("versi�n 2.0"); 
		
		System.out.println("Autor: David Guerrero Martinez");
	}
	/*
	 * esta llave corresponde al cierre del bloque del metodo main()
	 */
}
/*
esta llave corresponde al cierre de la clase "HolaMundo". Ambas deben tener siempre su pareja de apertura
*/

