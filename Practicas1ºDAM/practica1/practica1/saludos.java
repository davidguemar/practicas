package practica1;
// EJERCICIO 2
import java.util.Scanner;

public class saludos {
	public static void main(String[] args) {
		/* teclado es un canal de entrada por teclado a trav�s de un Scanner.
		   Necesita importar el paquete java.util
		 */
		Scanner teclado = new Scanner(System.in);

		int numOrden;			// Variable num�rica.
		String nombre = "David";		// Variable de texto. 

		// Configura y muestra mensajes de bienvenida.
		numOrden = 1 ;
		System.out.println("Hola,"); 
		System.out.print("Soy un modesto ");
		System.out.print("programa de ordenador...\n"); //   /n  Sirve para bajar la l�nea.
		System.out.println("Bienvenido al Curso.\t" + "Este es el saludo n� " + numOrden); //  /t Sirve para crear un espacio. 
		numOrden++;
		System.out.println("Bienvenido a Java.\t" + "Este es el saludo n� " + numOrden); // el signo +  las sirve para unir las frases.
		System.out.println("\nFin programa..."); 
	}
}
