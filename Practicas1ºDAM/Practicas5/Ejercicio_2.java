
public class Ejercicio_2 {

	public static void main(String[] args) {
		String texto = "((a + b) / 5-d)";
		parentesisCorrectos(texto);


	}

	public static boolean parentesisCorrectos(String texto) {
		boolean valor=false; 
		int contador= 0;
		for (int i=0; i < texto.length(); i++) {
			if (texto.charAt(i)=='(') {
				contador ++;
			}else if(texto.charAt(i)==')') {
				contador --;
			}
		}
		if (contador == 0) {
			valor = true;
		}

		return valor;
	}

}
