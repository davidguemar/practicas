/**
 * Ejercicio 04
 * Programa que implementa el m�todo removeAll, que sirve para poder
 * eliminar de la lista todos los elementos que se le proporcionen.
 * 
 *
 */

public class Ejercicio_04_ListaDinamica {

		// Atributos
		private Nodo primero;      // Referencia a nodo
		private int numElementos;

		// M�todos
		/**
		 * Constructor que inicializa los atributos al valor por defecto.
		 * Lista vac�a.
		 * @return 
		 */
		public  Ejercicio_04_ListaDinamica() {
			primero = null;
			numElementos = 0;
		}

		//...

		// class

		/** 
		 * Representa la estructura de un nodo para una lista din�mica con enlace simple.
		 */
		class Nodo {
			// Atributos
			Object dato;
			Nodo siguiente;	
			/**
			 * Constructor que inicializa atributos por defecto.
			 * @param elem - el elemento de informaci�n �til a almacenar.
			 */
			public Nodo(Object dato) {
				this.dato = dato;
				siguiente = null;
			}

		} // class
		/**
		 * A�ade un elemento al final de la lista.
		 * @param elem - el elemento a a�adir.
		 * Admite que el elemento a a�adir sea null.
		 */
		public void add(Object dato) {	 
			//variables auxiliares
			Nodo nuevo = new Nodo(dato);
			Nodo ultimo = null;
			if (numElementos == 0) {
				// Si la lista est� vac�a enlaza el nuevo nodo el primero.
				primero = nuevo;
			}
			else {
				// Obtiene el �ltimo nodo y enlaza el nuevo.
				ultimo = obtenerNodo(numElementos-1);
				ultimo.siguiente = nuevo;
			}
			numElementos++;               // Actualiza el n�mero de elementos.
		}

		/**
		 * Obtiene el nodo correspondiente al �ndice. Recorre secuencialmente la cadena de enlaces. 
		 * @param indice - posici�n del nodo a obtener.
		 * @return - el nodo que ocupa la posici�n indicada por el �ndice.
		 */
		private Nodo obtenerNodo(int indice) {
			assert indice >= 0 && indice < numElementos;
			// Recorre la lista hasta llegar al nodo  de posici�n buscada.
			Nodo actual = primero;
			for (int i = 0; i < indice; i++)
				actual = actual.siguiente;
			return actual;
		}
		/**
		 * Elimina el elemento indicado por el �ndice. Ignora �ndices negativos
		 * @param indice - posici�n del elemento a eliminar
		 * @return - el elemento eliminado o null si la lista est� vac�a.
		 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1
		 */
		public Object remove(int indice) {
			// Lanza excepci�n si el �ndice no es v�lido.
			if (indice >= numElementos || indice < 0) {
				throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
			}		
			if (indice > 0) {		
				return removeIntermedio(indice);
			}		
			if (indice == 0) {
				return removePrimero();
			}		
			return null;
		}
		/**
		 * Elimina el primer elemento.
		 * @return - el elemento eliminado o null si la lista est� vac�a.
		 */
		private Object removePrimero() {
			//variables auxiliares
			Nodo actual = null;
			actual = primero;			   // Guarda actual.
			primero = primero.siguiente;	   // Elimina elemento del principio.
			numElementos--;
			return actual.dato;
		}

		/**
		 * Elimina el elemento indicado por el �ndice. 
		 * @param indice - posici�n del elemento a eliminar.
		 * @return - el elemento eliminado o null si la lista est� vac�a.
		 */
		private Object removeIntermedio(int indice) {
			assert indice > 0 && indice < numElementos;
			//variables auxiliares
			Nodo actual = null;
			Nodo anterior = null;
			// Busca nodo del elemento anterior correspondiente al �ndice.
			anterior = obtenerNodo(indice - 1);
			actual = anterior.siguiente;		     // Guarda actual.
			anterior.siguiente = actual.siguiente;      // Elimina el elemento.
			numElementos--;
			return actual.dato;	
		}
		// Elimina elementos de una colecci�n contenidos en otra.
		public Object removeAll(int indice) {
			// Lanza excepci�n si el �ndice no es v�lido.
			if (indice >= numElementos || indice < 0) {
				throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
			}		
			if (indice > 0) {		
				return removeIntermedio(indice);
			}		
			if (indice == 0) {
				return removePrimero();
			}		
			return null;
		}

		
		
		
		/**
		 * Elimina el dato especificado.
		 * @param dato � a eliminar.
		 * @return - el �ndice del elemento eliminado o -1 si no existe.
		 */
		public int remove(Object dato) { 
			// Obtiene el �ndice del elemento especificado.
			int actual = indexOf(dato);
			if (actual != -1) {
				remove(actual);        // Elimina por �ndice.
			}
			return actual;
		}

		/**
		 * Busca el �ndice que corresponde a un elemento de la lista.
		 * @param dato- el objeto elemento a buscar.
		 */
		public int indexOf(Object dato) {
			Nodo actual = primero;
			for (int i = 0; actual != null; i++) {
				if ((actual.dato != null && actual.dato.equals(dato))
						|| actual.dato == dato) {
					return i;
				}
				actual = actual.siguiente;
			}
			return -1;
		}
		/**
		 * @param indice � obtiene un elemento por su �ndice.
		 * @return elemento contenido en el nodo indicado por el �ndice.
		 * @exception IndexOutOfBoundsException - �ndice no est� entre 0 y numElementos-1.
		 */
		public Object get(int indice) {
			// lanza excepci�n si el �ndice no es v�lido
			if (indice >= numElementos || indice < 0) {
				throw new IndexOutOfBoundsException("�ndice incorrecto: " + indice);
			}
			Nodo aux = obtenerNodo(indice);
			return aux.dato;
		} 

		/**
		 * @return el n�mero de elementos de la lista
		 */
		public int size() {
			return numElementos;
		}
		public static class PruebaListaEnlazada {
			public static void main(String[] args) {
				Ejercicio_04_ListaDinamica listaCompra = new Ejercicio_04_ListaDinamica();
				listaCompra.add("Leche");
				listaCompra.add("Miel");
				listaCompra.add("Aceitunas");
				listaCompra.add("Cerveza");
				listaCompra.add("Caf�");
				System.out.println("Lista de la compra:");
				for (int i = 0; i < listaCompra.size(); i++) {
					System.out.println(listaCompra.get(i));
				}
				System.out.println("elementos en la lista: " + listaCompra.size());
				System.out.println("elementos 3 en la lista: " + listaCompra.get(3));
				System.out.println("posici�n del elemento Miel: " + listaCompra.indexOf("Miel"));
				System.out.println("eliminado: " + listaCompra.remove("Miel"));
			}
		}


	}

