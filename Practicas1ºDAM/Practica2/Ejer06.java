/**
 *  Programa que declara 3 variables de tipo entero, asigna valor diferente a cada una y las muestra en pantalla. 
 */

public class Ejer06 {
	
	public static void main(String[] args) {
		
		int dato1 =2;
		int dato2 =3;
		int dato3 =1;
		//variables declaradas
		
		System.out.println(dato1);
		System.out.println(dato2);
		System.out.println(dato3);
	}
}
