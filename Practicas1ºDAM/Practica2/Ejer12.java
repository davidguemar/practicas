/**
 * Programa simple que lee exactamente ocho n�meros enteros y luego escribe la suma de todos ellos.
 * @author david
 *
 */
import java.util.Scanner;

public class Ejer12 {

	public static void main(String[] args) {
		
	     int numeros = 0;
	        int suma = 0;
	        Scanner sc= new Scanner(System.in);
	        
	        System.out.println("introduce ocho n�meros");
	         
	        while(numeros < 8) {
	            numeros++;// el bucle while se va a repetir hasta que llegue a 8 cada vez que se repita con el operando ++ se sumar� 1 hata llegar a 8
	            suma += sc.nextInt();//la variable suma se va incrementando segun el n�mero que introduzcamos en nextInt, de ah� el resultado de la suma 
	        }
	         
	        System.out.println("La suma total es: "+suma);
		
		sc.close();
	

	}

}
