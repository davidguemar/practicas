

/**
 * Programa que pide tres datos de tipo entero por teclado, los guarda y los muestra por teclado 
 * 
 * @author david
 *
 */
import java.util.Scanner;
public class Ejer10 {

	
	
	public static void main(String[] args) {
		
		Scanner sc= new Scanner(System.in);
	
		
		System.out.println("Dame un n�mero");
		int dato1 =sc.nextInt();
		
		System.out.println("Dame otro n�mero");
		int dato2 =sc.nextInt();
		
		System.out.println("Dame un n�mero m�s");
		int dato3 =sc.nextInt();
		
		
		//El valor de los datos se almacena en las variables por pantalla, y luego los ordena.
		
		System.out.println("Los n�meros de manera ordenada son...");
		
		
		
		if (dato1 < dato2 && dato1 < dato3) {
			System.out.println(dato1);
			if (dato2 < dato3) { 
				System.out.println(dato2);
				System.out.println(dato3);	
			} else {
				System.out.println(dato3);
				System.out.println(dato2);
		
			}// Si el dato menor es el primero se ejecuta este bucle, ordenando los otros dos valores 
			
		}
		
		if (dato2 < dato1 && dato2 < dato3) {
			System.out.println(dato2);
			if (dato1 < dato3) {
				System.out.println(dato1);
			System.out.println(dato3);
			} else {
			System.out.println(dato3);
			System.out.println(dato1);
			
		    }//Si el dato menor es este, se ejecuta este bucle, ordenando a continuaci�n los otros dos datos.
			
		}
		
		if (dato3 < dato1 && dato3 < dato2) {
			System.out.println(dato3);
			if (dato1 < dato2) {
				System.out.println(dato1);
				System.out.println(dato2);
			} else {
				System.out.println(dato2);
				System.out.println(dato1);
				sc.close();
			}// El mismo procedimiento, si el dato menor es el dato3 se ejecuta este bucle y a continuaci�n ordena los otros dos n�meros por descarte.
			
		}
      
		
	}

}
