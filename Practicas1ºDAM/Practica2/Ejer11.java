/**
 * Programa simple que calcula el inter�s producido y el capital total acumulado de un capital incial invertido a un tipo de inter�s anual. 
 * @author david
 *
 */
import java.util.Scanner;

public class Ejer11 {

	public static void main(String[] args) {
	
		
		
		
			Scanner sc=new Scanner(System.in);
		
		System.out.println("Introduce el capital inicial");
		
		     double CapitalInicial=sc.nextDouble();		
		
		System.out.println("Introduce el inter�s anual");
		
		    double Inter�sAnual=sc.nextDouble();		
		
		System.out.println("Introduce el n�mero de a�os");
		
		    int N�meroA�os=sc.nextInt();		
		
		double resultado= CapitalInicial * Math.pow((Inter�sAnual/100), N�meroA�os);//formula para calcular el capital seg�n el ejercicio
				
	    System.out.println("El resultado es: \n " + resultado );
	    
		sc.close();	//cerramos la entrada por teclado

	}

}
