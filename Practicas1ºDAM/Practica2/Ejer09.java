/**
 * Programa simple que muestra iniciales del nombre en pantalla mas o menos centradas con una medida de 7x7.
 * @author david
 *
 */

public class Ejer09 {
	
	public static void main(String[] args) {
		System.out.print("D D  D             G G G G G G       M              M \n");
		System.out.print("D       D        G            G      M  M        M  M\n");
		System.out.print("D         D     G                    M     M   M    M\n");
		System.out.print("D         D     G                    M       M      M\n");
		System.out.print("D         D     G       G  G  G      M              M\n");
		System.out.print("D       D        G             G     M              M\n");
		System.out.print("D D  D             G G G G G G       M              M\n");
	}

}
