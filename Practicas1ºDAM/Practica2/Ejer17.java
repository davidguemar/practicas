/*
 * programa simple que pide un n�mero entero y a partir de �l crea en pantalla de texto un cuadrado de asteriscos con ese tama�o de lado.
 * 
 */
import java.util.Scanner;
public class Ejer17 {

	public static void main(String[] args) {
		   Scanner sc = new Scanner(System.in);
	       
	        System.out.print("Por favor escribe el tama�o del cuadrado: ");
	        int n = sc.nextInt();
	       
	        if(n >= 0 && n<=50) {
	            //Linea superior
	            for(int i = 0; i < n; i++) {
	                System.out.print("* ");
	            }
	            System.out.println();
	           
	            //centro de la forma
	            for(int i = 0; i < n-2; i++) {
	                System.out.print("* ");
	                for(int j = 0; j < n-2; j++) {
	                    System.out.print("  ");
	                }
	                System.out.println("* ");
	            }
	           
	            //Linea inferior
	            for(int i = 0; i < n; i++) {
	                System.out.print("* ");
	            }
	        }else {
	            System.out.println("Error. El dato a ingresar de "
	                    + "estar entre 0 y 50");
	            sc.close();
	        }
	               
	 

	}

}
