/*
 * Programa simple que pide un n�mero entero por teclado y calcula su factorial.
 * 
 */
import java.util.Scanner;
public class Ejer18 {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		int resultado=1;
		
		System.out.println("Introduce un n�mero positivo");
		
		int numero=sc.nextInt();
		//bucle for
		for( int i=numero; i>0;i--) {
			
			resultado=resultado*i; //cada vez que pasa por aqui va multiplicanco el n�mero para sacar el factorial
		}
		
		System.out.println("El factorial de " + numero+ " es " + resultado);
            
		sc.close();
	}

}
