/*
 * Programa simple que lee una serie de numeros por teclado e indica cual es el mayor de ellos 
 */
import java.util.Scanner;
public class Ejer13 {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		
		int n1, n2;
		n1=0;
		char continuar;
		//bucle do while
		do {
			System.out.println("Introduce un n�mero");
			n2=sc.nextInt();
			
			if (n2>n1) {
				n1=n2;
			}
			System.out.println("pulsa S para continuar o cualquier tecla para finalizar");
			continuar = sc.next().charAt(0);
		}while (continuar =='s'); // el programa continua mientras se pulse la tecla s
		System.out.println("fin del ejercicio el n�mero mayor es "+ n1);
		
		sc.close();
		

	}

}